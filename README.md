# Awesome Color Palettes

A collection of handpicked color palettes, ready to copy into your project.

## Contents

- [Tailwind CSS](palettes/tailwind.md)